import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { CREATE_LINK_MUTATION, ALL_LINKS_QUERY } from '../../graphql';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-create-link',
  templateUrl: './create-link.component.html',
  styleUrls: ['./create-link.component.scss']
})
export class CreateLinkComponent implements OnInit {
  // tslint:disable-next-line:no-inferrable-types
  description: string = '';
  // tslint:disable-next-line:no-inferrable-types
  url: string = '';

  constructor(public apollo: Apollo, private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  createLink() {
    const postedById = this.authService.userId;
    if (!postedById) {
      return console.error('No user logged in');
    }

    const newDescription = this.description;
    const newUrl = this.url;
    this.description = '';
    this.url = '';

    this.apollo.mutate({
      mutation: CREATE_LINK_MUTATION,
      variables: {
        description: newDescription,
        url: newUrl,
        postedById
      },
      update: (store, mutationResult) => {
        const createLink = mutationResult.data['createVote'];
        const data: any = store.readQuery({
          query: ALL_LINKS_QUERY
        });

        data.allLinks.push(createLink);
        store.writeQuery({ query: ALL_LINKS_QUERY, data });
      },
    }).subscribe((response) => {
      // We injected the Router service
      this.router.navigate(['/']);
    }, (error) => {
      console.error(error);
      this.description = newDescription;
      this.url = newUrl;
    });
}

}
