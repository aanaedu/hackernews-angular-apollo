import { Component, OnInit, OnDestroy } from '@angular/core';
import { Link } from 'src/app/types';
import { Subscription } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { AuthService } from 'src/app/auth/auth.service';
import { ALL_LINKS_SEARCH_QUERY } from 'src/app/graphql';

@Component({
  selector: 'app-search-link',
  templateUrl: './search-link.component.html',
  styleUrls: ['./search-link.component.scss']
})
export class SearchLinkComponent implements OnInit, OnDestroy {
  allLinks: Link[] = [];
  loading: boolean = true;
  searchText: string = '';

  loggedIn: boolean = false;

  subscriptions: Subscription[] = [];

  constructor(private apollo: Apollo, private authService: AuthService) {
  }

  ngOnInit() {

    this.authService.isAuthenticated$
      // .distinctUntilChanged()
      .subscribe(isAuthenticated => {
        this.loggedIn = isAuthenticated;
      });

  }

  // 3
  executeSearch() {
    if (!this.searchText || this.searchText.length < 3) {
      return;
    }

    const querySubscription = this.apollo.watchQuery({
      query: ALL_LINKS_SEARCH_QUERY,
      variables: {
        searchText: this.searchText
      },
    }).valueChanges.subscribe((response: any) => {
      this.allLinks = response.data.allLinks;
      this.loading = response.data.loading;
    });

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  ngOnDestroy(): void {
    for (const sub of this.subscriptions) {
      if (sub && sub.unsubscribe) {
        sub.unsubscribe();
      }
    }
  }
}
