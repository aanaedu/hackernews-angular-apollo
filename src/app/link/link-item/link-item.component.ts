import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Link } from '../../types';
import { Subscription } from 'rxjs';
import { timeDifferenceForDate } from '../../common/utils';
import { AuthService } from 'src/app/auth/auth.service';
import { Apollo } from 'apollo-angular';
import { CREATE_VOTE_MUTATION } from 'src/app/graphql';
import { DataProxy } from 'apollo-cache';
import { FetchResult } from 'apollo-link';


interface UpdateStoreAfterVoteCallback {
  (proxy: DataProxy, mutationResult: FetchResult, linkId: string);
}

@Component({
  selector: 'app-link-item',
  templateUrl: './link-item.component.html',
  styleUrls: ['./link-item.component.scss']
})
export class LinkItemComponent implements OnInit, OnDestroy {

  @Input()
  link: Link;

  @Input()
  index: number = 0;

  @Input()
  isAuthenticated: boolean = false;

  @Input()
  updateStoreAfterVote: UpdateStoreAfterVoteCallback;

  subscriptions: Subscription[] = [];


  constructor(private apollo: Apollo, private authService: AuthService) { }

  ngOnInit() {
  }

  voteForLink = async () => {
    const userId = this.authService.userId;
    const voterIds = this.link.votes.map(vote => vote.user.id);
    if (voterIds.includes(userId)) {
      alert(`User (${userId}) already voted for this link.`);
      return;
    }
    const linkId = this.link.id;

    const mutationSubscription = this.apollo.mutate({
      mutation: CREATE_VOTE_MUTATION,
      variables: {
        userId,
        linkId
      },
      update: (store, mutationResult) => {
        const createVote = mutationResult.data['createVote'];
        this.updateStoreAfterVote(store, createVote, linkId);
      }
    })
      .subscribe();

    this.subscriptions = [...this.subscriptions, mutationSubscription];
  }

  humanizeDate(date: string) {
    return timeDifferenceForDate(date);
  }

  ngOnDestroy(): void {
    for (const sub of this.subscriptions) {
      if (sub && sub.unsubscribe) {
        sub.unsubscribe();
      }
    }
  }

}
