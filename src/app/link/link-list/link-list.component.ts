import { Component, OnInit, OnDestroy } from '@angular/core';
import { Link } from '../../types';
import { Apollo } from 'apollo-angular';
import { ALL_LINKS_QUERY } from '../../graphql';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-link-list',
  templateUrl: './link-list.component.html',
  styleUrls: ['./link-list.component.scss']
})
export class LinkListComponent implements OnInit, OnDestroy {
  allLinks: Link[];
  loading: boolean = true;
  loggedIn: boolean = false;
  subscriptions: Subscription[] = [];

  constructor(private apollo: Apollo, private authService: AuthService) {}

  ngOnInit() {
    this.authService.isAuthenticated$
      // .distinctUntilChanged()
      .subscribe(isAuthenticated => {
        this.loggedIn = isAuthenticated;
      });

    const querySubscription = this.apollo.watchQuery({
      query: ALL_LINKS_QUERY
    }).valueChanges.subscribe( (response: any) => {
      this.allLinks = response.data.allLinks;
      this.loading = response.data.loading;
    });

    this.subscriptions = [...this.subscriptions, querySubscription];
  }


  updateStoreAfterVote(store: any, createVote: any, linkId: string): any {
    // read current state of the cached data
    const data = store.readQuery({
      query: ALL_LINKS_QUERY
    });
    // update value
    const votedLink = data.allLinks.find((link: any) => link.id === linkId);
    votedLink.votes = createVote.link.votes;

    // write modified data back into store
    store.writeQuery({ query: ALL_LINKS_QUERY, data });
  }

  ngOnDestroy(): void {
    for (const sub of this.subscriptions) {
      if (sub && sub.unsubscribe) {
        sub.unsubscribe();
      }
    }
  }

}
