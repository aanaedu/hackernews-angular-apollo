import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphlQLModule } from './apollo.config';
import { LinkItemComponent } from './link/link-item/link-item.component';
import { LinkListComponent } from './link/link-list/link-list.component';
import { CreateLinkComponent } from './link/create-link/create-link.component';
import { HeaderComponent } from './layout/header/header.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthService } from './auth/auth.service';
import { SearchLinkComponent } from './link/search-link/search-link.component';

@NgModule({
  declarations: [
    AppComponent,
    LinkItemComponent,
    LinkListComponent,
    CreateLinkComponent,
    HeaderComponent,
    LoginComponent,
    SearchLinkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphlQLModule,
    FormsModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
