import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedIn: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.isAuthenticated$
      // .distinctUntilChanged()
      .subscribe((isAuthenticated: any) => {
        this.loggedIn = isAuthenticated;
      });
  }

  logout() {
    this.authService.logout();
  }

}
