import { NgModule } from '@angular/core';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';

import { Apollo, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { environment } from 'src/environments/environment';
import { ApolloLink } from 'apollo-link';
import { GC_AUTH_TOKEN } from './constants';

@NgModule({
    exports: [
        HttpClientModule,
        ApolloModule,
        HttpLinkModule
    ]
})
export class GraphlQLModule {
    constructor(apollo: Apollo, httpLink: HttpLink) {
        const uri = environment.graphqlEndpoint;
        const http = httpLink.create({ uri });
        // configure apollo with the Auth Token
        const middleware = new ApolloLink((operation, forward) => {
            const token = localStorage.getItem(GC_AUTH_TOKEN);
            if (token) {
                operation.setContext({
                    headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
                });
            }
            return forward(operation);
        });

        apollo.create({
            link: middleware.concat(http),
            cache: new InMemoryCache()
        });
    }
}
