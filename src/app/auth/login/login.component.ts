import { Component, OnInit } from '@angular/core';
import { GC_USER_ID, GC_AUTH_TOKEN } from 'src/app/constants';
import { AuthService } from '../auth.service';
import { Apollo } from 'apollo-angular';
import { Router } from '@angular/router';
import { SIGNIN_USER_MUTATION, CREATE_USER_MUTATION } from 'src/app/graphql';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: boolean = true; // switch between Login and SignUp
  email: string = '';
  password: string = '';
  name: string = '';
  formSubmitted: boolean = false;

  constructor(
    private router: Router,
    private apollo: Apollo,
    private authService: AuthService) {
  }


  ngOnInit() {
  }

  getPointEventValue() {
    return this.formSubmitted ? 'none' : 'auto';
  }

  confirm() {
    this.formSubmitted = true;
    if (this.login) {
      this.apollo.mutate({
        mutation: SIGNIN_USER_MUTATION,
        variables: {
          email: this.email,
          password: this.password
        }
      }).subscribe((result: any) => {
        const id = result.data.authenticateUser.id;
        const token = result.data.authenticateUser.token;
        this.saveUserData(id, token);

        this.router.navigate(['/']);
      }, (error) => {
        alert(error);
        this.formSubmitted = false;
      });
    } else {
      this.apollo.mutate({
        mutation: CREATE_USER_MUTATION,
        variables: {
          email: this.email,
          password: this.password,
          name: this.name
        }
      }).subscribe((result: any) => {
        const id = result.data.authenticateUser.id;
        const token = result.data.authenticateUser.token;
        this.saveUserData(id, token);

        this.router.navigate(['/']);
      }, (error) => {
        alert(error);
        this.formSubmitted = false;
      });
    }
  }

  saveUserData(id, token) {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
    this.authService.setUserId(id);
  }

}
