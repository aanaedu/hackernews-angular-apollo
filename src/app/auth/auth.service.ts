import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _userId: string = null;

  private _isAuthenticated = new BehaviorSubject(false);

  constructor() { }

  get isAuthenticated$() {
    return this._isAuthenticated.asObservable();
  }

  get userId() {
    return localStorage.getItem(GC_USER_ID);
  }

  saveUserData(id: string, token: string) {
    localStorage.setItem(GC_USER_ID, id);
    localStorage.setItem(GC_AUTH_TOKEN, token);
    this.setUserId(id);
  }

  setUserId(id: string): any {
    this._userId = id;
    this._isAuthenticated.next(true);
  }

  logout() {
    localStorage.removeItem(GC_USER_ID);
    localStorage.removeItem(GC_AUTH_TOKEN);
    this._userId = null;

    this._isAuthenticated.next(false);
  }

  autoLogin() {
    const id = localStorage.getItem(GC_USER_ID);
    if (id) {
      this.setUserId(id);
    }
  }
}
