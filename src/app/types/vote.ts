import { User } from './user';
import { Link } from './link';

export class Vote {
    id?: string;
    user?: User;
    link?: Link;
}
