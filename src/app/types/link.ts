import { User } from './user';
import { Vote } from './vote';

export class Link {
    id?: string;
    description?: string;
    url?: string;
    createdAt?: string;
    postedBy?: User;
    votes?: Vote[];
}
