import { Vote } from './vote';

export class User {
    id?: string;
    email?: string;
    password?: string;
    name?: string;
    votes?: Vote[];
}
