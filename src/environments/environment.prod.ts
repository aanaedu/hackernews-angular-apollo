export const environment = {
  production: true,
  graphqlEndpoint: 'https://api.graph.cool/simple/v1/cjrgxfrxof0ne01019yhvjoy2',
  graphqlSubscriptionEndpoint: 'wss://subscriptions.us-west-2.graph.cool/v1/cjrgxfrxof0ne01019yhvjoy2'
};
